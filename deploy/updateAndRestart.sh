#!/bin/bash

# any future command that fails will exit the script
set -e

echo "Installing ssb19_iot_api"

# echo "Removing old code..."
# Delete the old repo
# rm -rf ssb19_iot_api

echo "Node Check!"
node -v
echo "NPM Check!"
npm -v
echo "Node PM2 Check!"
pm2 -v

# stop the previous pm2
# echo "PM2 Stop all"
# pm2 stop all
# echo "PM2 Kill"
# pm2 kill
echo "PM2 Status"
pm2 status
# npm remove pm2 -g

#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
# npm install pm2 -g
# starting pm2 daemon

cd ssb19_iot_api

echo "Cloning repo..."
# clone the repo again
git pull 

#install npm packages
echo "Running npm install"
npm install

#Restart the node server
npm start